const btn = document.querySelector(".btn-toggle");

btn.addEventListener("click", function () {
  document.getElementById("myBody").classList.toggle("dark-theme");
});

const menu = document.getElementById("menu");
const btnMenu = document.getElementById("btn-menu");
const icon_delete = document.getElementById("icon_delete");

btnMenu.addEventListener("click", () => {
  menu.classList.add("show");
});
icon_delete.addEventListener("click", () => {
  menu.classList.remove("show");
});
